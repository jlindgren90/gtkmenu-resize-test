CFLAGS := -Wall -O2
GTK2_CFLAGS := $(shell pkg-config --cflags gtk+-2.0)
GTK3_CFLAGS := $(shell pkg-config --cflags gtk+-3.0)
GTK2_LIBS := $(shell pkg-config --libs gtk+-2.0)
GTK3_LIBS := $(shell pkg-config --libs gtk+-3.0)

all: menu-gtk2 menu-gtk3 statusmenu-gtk2 statusmenu-gtk3

menu-gtk2: menu.c
	gcc ${CFLAGS} ${GTK2_CFLAGS} -o menu-gtk2 menu.c ${GTK2_LIBS}
menu-gtk3: menu.c
	gcc ${CFLAGS} ${GTK3_CFLAGS} -o menu-gtk3 menu.c ${GTK3_LIBS}
statusmenu-gtk2: statusmenu.c
	gcc ${CFLAGS} ${GTK2_CFLAGS} -o statusmenu-gtk2 statusmenu.c ${GTK2_LIBS}
statusmenu-gtk3: statusmenu.c
	gcc ${CFLAGS} ${GTK3_CFLAGS} -o statusmenu-gtk3 statusmenu.c ${GTK3_LIBS}

clean:
	rm -rf menu-gtk2 menu-gtk3 statusmenu-gtk2 statusmenu-gtk3
