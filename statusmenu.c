#define GLIB_VERSION_MIN_REQUIRED GLIB_VERSION_2_32
#define GDK_VERSION_MIN_REQUIRED GDK_VERSION_3_0
#include <gtk/gtk.h>
#include <stdio.h>

static gboolean add_more_items(GtkWidget * menu)
{
    for (int i = 5; i < 10; i++)
    {
        char name[10];
        snprintf(name, sizeof(name), "Item %d", i);
        GtkWidget * menuitem = gtk_menu_item_new_with_label(name);
        gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
    }

    gtk_widget_show_all(menu);
    gtk_menu_reposition(GTK_MENU(menu));

    return G_SOURCE_REMOVE;
}

static void show_menu(GtkWidget * icon, GtkWidget * menu)
{
    gtk_widget_show_all(menu);
    gtk_menu_popup(GTK_MENU(menu), NULL, NULL, gtk_status_icon_position_menu,
                   icon, 0, gtk_get_current_event_time());
}

int main(void)
{
    gtk_init(NULL, NULL);

    GtkStatusIcon * icon = gtk_status_icon_new_from_icon_name("go-up");
    GtkWidget * menu = gtk_menu_new();

    for (int i = 0; i < 5; i++)
    {
        char name[10];
        snprintf(name, sizeof(name), "Item %d", i);
        GtkWidget * menuitem = gtk_menu_item_new_with_label(name);
        gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
    }

    g_signal_connect(icon, "activate", (GCallback)show_menu, menu);
    g_timeout_add(5000, (GSourceFunc)add_more_items, menu);

    gtk_main();

    return 0;
}
