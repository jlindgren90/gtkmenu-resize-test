#define GLIB_VERSION_MIN_REQUIRED GLIB_VERSION_2_32
#include <gtk/gtk.h>
#include <stdio.h>

static gboolean add_more_items(GtkWidget * submenu)
{
    for (int i = 5; i < 10; i++)
    {
        char name[10];
        snprintf(name, sizeof(name), "Item %d", i);
        GtkWidget * submenuitem = gtk_menu_item_new_with_label(name);
        gtk_menu_shell_append(GTK_MENU_SHELL(submenu), submenuitem);
    }

    gtk_widget_show_all(submenu);
    gtk_menu_reposition(GTK_MENU(submenu));

    return G_SOURCE_REMOVE;
}

int main(void)
{
    gtk_init(NULL, NULL);

    GtkWidget * window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GtkWidget * menubar = gtk_menu_bar_new();
    GtkWidget * menuitem = gtk_menu_item_new_with_label("Menu");
    GtkWidget * submenu = gtk_menu_new();

    for (int i = 0; i < 5; i++)
    {
        char name[10];
        snprintf(name, sizeof(name), "Item %d", i);
        GtkWidget * submenuitem = gtk_menu_item_new_with_label(name);
        gtk_menu_shell_append(GTK_MENU_SHELL(submenu), submenuitem);
    }

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuitem);
    gtk_container_add(GTK_CONTAINER(window), menubar);
    gtk_widget_show_all(window);

    g_timeout_add(5000, (GSourceFunc)add_more_items, submenu);
    g_signal_connect(window, "destroy", (GCallback)gtk_main_quit, NULL);

    gtk_main();

    return 0;
}
